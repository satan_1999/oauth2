package au.com.bgl.api.oauthprovider.endpoint;

import java.security.Principal;
import java.util.Map;

import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.endpoint.AuthorizationEndpoint;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import au.com.bgl.api.oauthprovider.exceptions.OAuth2AuthorizationEndpointException;

public class WrappedAuthorizationEndpoint extends AuthorizationEndpoint {

    @Override
    public ModelAndView authorize(Map<String, Object> model,
          @RequestParam Map<String, String> requestParameters, SessionStatus sessionStatus, Principal principal) {
        try {
            return super.authorize(model, requestParameters, sessionStatus, principal);
        }
        catch (ClientRegistrationException e) {
            throw new OAuth2AuthorizationEndpointException(e);
        }
        catch(OAuth2Exception e) {
            throw new OAuth2AuthorizationEndpointException(e);
        }
    }
}
