package au.com.bgl.api.auth

class ApiClient {

  private static final String NO_CLIENT_SECRET = ''

  transient springSecurityService

  String clientId
  String clientSecret

  Integer accessTokenValiditySeconds
  Integer refreshTokenValiditySeconds

  Map<String, Object> additionalInformation

  static hasMany = [
    authorities: String,
    authorizedGrantTypes: String,
    resourceIds: String,
    scopes: String,
    redirectUris: String
  ]
  static mapping = {
    authorities lazy: false
    authorizedGrantTypes lazy: false
    resourceIds lazy: false
    scopes lazy: false
    redirectUris lazy: false
}
  static transients = ['springSecurityService']

  static constraints = {
    clientId blank: false, unique: true
    clientSecret nullable: true

    accessTokenValiditySeconds nullable: true
    refreshTokenValiditySeconds nullable: true

    authorities nullable: true
    authorizedGrantTypes nullable: true

    resourceIds nullable: true
    scopes nullable: true

    redirectUris nullable: true
    additionalInformation nullable: true
  }
  List authorities
  List authorizedGrantTypes
  List resourceIds
  List scopes
  List redirectUris

  def beforeInsert() {
    encodeClientSecret()
  }

  def beforeUpdate() {
    if(isDirty('clientSecret')) {
      encodeClientSecret()
    }
  }

  protected void encodeClientSecret() {
    clientSecret = clientSecret ?: NO_CLIENT_SECRET
    clientSecret = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(clientSecret) : clientSecret
  }
}
