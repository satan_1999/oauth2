import org.springframework.security.web.SecurityFilterChain

import grails.plugin.springsecurity.SpringSecurityUtils
import grails.util.Environment

import javax.servlet.Filter

import org.springframework.beans.MutablePropertyValues
import org.springframework.beans.factory.config.ConstructorArgumentValues
import org.springframework.beans.factory.config.RuntimeBeanReference
import org.springframework.beans.factory.support.AbstractBeanDefinition
import org.springframework.beans.factory.support.GenericBeanDefinition
import org.springframework.security.web.DefaultSecurityFilterChain
import org.springframework.security.web.FilterChainProxy
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.context.SecurityContextPersistenceFilter
import org.springframework.security.web.util.matcher.AntPathRequestMatcher

import au.com.bgl.api.auth.ApiClient
import au.com.bgl.api.auth.Book
import au.com.bgl.api.auth.Role
import au.com.bgl.api.auth.User
import au.com.bgl.api.auth.UserRole

class BootStrap {
  def grailsApplication
  def init = { servletContext ->

    /*grant_type=authorization_code
     * 1.URL Request:
     http://localhost:7676/OAuth2Demo/oauth/authorize?response_type=code&client_id=my-client&scope=read
     * 2.URL Request:
     curl -v -X POST \
        -H "Content-Type: application/json" \
        -H "Authorization: Basic $2a$10$8bsVUn.JLTEDKgxQ1WggYeKKySHsPh8G.EUaevib0j1D2dcLl6dTa" \
        'http://localhost:7676/OAuth2Demo/oauth/token?grant_type=authorization_code&code=IEzrcD&client_id=my-client&scope=read&redirect_uri=http://localhost:7677/OAuth2DemoClient/test/verify'
    
    
      curl -v -X POST \
        -H "Content-Type: application/json" \
        'http://localhost:7676/OAuth2Demo/oauth/token?grant_type=authorization_code&code=S6HCJK&client_id=my-client&scope=read&redirect_uri=http://localhost:7677/OAuth2DemoClient/test/verify'
    
    http://localhost:7676/OAuth2Demo/oauth/token?grant_type=authorization_code&code=inYOLd&client_id=my-client&scope=read&redirect_uri=http://localhost:7677/OAuth2DemoClient/test/verify
    
    
     * 3.Resource Request:
     curl -v -X GET \
     -H "Content-Type: application/json" \
     -H "Authorization: Bearer 902e318f-d91b-40dd-9651-7ea4e1ce4da4" \
     'http://localhost:7676/OAuth2Demo/book/list.json'
     */
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"))
    switch (Environment.current.name) {
      case Environment.DEVELOPMENT.name:
        if(grailsApplication.config.dataSource.dbCreate in ['create', 'create-drop']){
          System.setProperty("h2.storeLocalTime", "true") // H2 in-memory database stuffs up the timezone if we don't have this
          mockData()
        }
        break
      case "ci":
        if(grailsApplication.config.dataSource_agLite.dbCreate in ['create', 'create-drop']){
          mockData()
        }
        break
      case "ci-cf":
        break
      case Environment.TEST.name:
        if(grailsApplication.config.dataSource_dmp.dbCreate in ['create', 'create-drop']){
          System.setProperty("h2.storeLocalTime", "true") // H2 in-memory database stuffs up the timezone if we don't have this
          mockData()
        }


        break
      default:
        break
    }
  }
  private void mockData(){
    Role role =  Role.findOrSaveByAuthority('ROLE_USER').save(flush:true)
    User user = new User(
            username:"bob",
            password:"pass",
            enabled:true,
            accountExpired:false,
            accountLocked:false,
            passwordExpired:false
            ).save(flush:true)
    UserRole.create(user, role, true)

    new ApiClient(
            clientId: 'my-client',
            // clientSecret : 'clientSecret',
            authorizedGrantTypes: [
              'authorization_code',
              'refresh_token',
              'implicit',
              'password',
              'client_credentials'
            ],
            authorities: ['ROLE_CLIENT'],
            scopes: ['read'],
            redirectUris: [
              'http://localhost:7677/OAuth2DemoClient/test/verify'
            ]).save(flush: true)
    //Mock Books
    new Book(name:'testBook',author:'BGL').save()
    new Book(name:'testBook1',author:'BGL').save()
    new Book(name:'testBook2',author:'BGL').save()
  }


  def destroy = {
  }
}
