
import grails.util.Environment

grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.server.port.http = 7676
//grails.project.war.file = "target/${appName}-${appVersion}.war"

// Code Narc
codenarc.reports = {
  XmlReport('xml') {
    outputFile = 'target/test-reports/CodeNarcReport.xml'
    title = 'OAuth2 Report'
  }
  HtmlReport('html') {
    outputFile = 'target/test-reports/CodeNarcReport.html'
    title = 'OAuth2 Report'
  }
}

grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
  // inherit Grails' default dependencies
  inherits("global") {
    // specify dependency exclusions here; for example, uncomment this to disable ehcache:
    // excludes 'ehcache'
  }
  log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
  checksums true // Whether to verify checksums on resolve
  legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

  repositories {
    inherits true // Whether to inherit repository definitions from plugins

    grailsPlugins()
    grailsHome()
    mavenLocal()
    grailsCentral()
    mavenCentral()
    // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
    //mavenRepo "http://repository.codehaus.org"
    //mavenRepo "http://download.java.net/maven/2/"
    //mavenRepo "http://repository.jboss.com/maven2/"
    mavenRepo "http://central.maven.org/maven2/"
    mavenRepo "http://repo.spring.io/milestone/"
    mavenRepo "http://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-support"
    mavenRepo "http://repo.bglcorp.com.au:8081/content/repositories/thirdparty/"

  }

  dependencies {
    // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
    // runtime 'mysql:mysql-connector-java:5.1.29'
    // runtime 'org.postgresql:postgresql:9.3-1101-jdbc41'
    compile('org.codehaus.groovy.modules.http-builder:http-builder:0.7.1') { excludes "commons-logging",  "groovy" }
    compile 'log4j:log4j:1.2.17',
            'commons-httpclient:commons-httpclient:3.1',
            'commons-net:commons-net:3.0.1',
            'org.codehaus.jackson:jackson-mapper-asl:1.9.8',
            'org.codehaus.jackson:jackson-core-asl:1.9.8',
            "joda-time:joda-time:2.1"
    runtime "net.sourceforge.jtds:jtds:1.2.4","org.jasypt:jasypt:1.7.1"
    test "org.grails:grails-datastore-test-support:1.0-grails-2.4"
    compile 'org.springframework.security.oauth:spring-security-oauth2:2.0.2.RELEASE', {
      excludes "spring-beans",
              "spring-core",
              "spring-context",
              "spring-aop",
              "spring-jdbc",
              "spring-webmvc",
              "spring-security-core",
              "spring-security-config",
              "spring-security-web",
              "spring-tx",
              "commons-codec"
    }
    test "org.gebish:geb-spock:0.9.3"
    test "org.gebish:geb-junit4:0.9.3"
    test "org.apache.httpcomponents:httpclient:4.3.2", { export = false }
    test "org.seleniumhq.selenium:selenium-support:2.40.0"
    test "org.seleniumhq.selenium:selenium-chrome-driver:2.40.0", { export = false }

  }

  plugins {
    // plugins for the build system only
    build ":tomcat:7.0.55"

    // plugins for the compile step
    compile ":scaffolding:2.1.2"
    compile ':cache:1.1.7'
    compile ":asset-pipeline:1.9.6"
    //compile ":dbconsole:1.1"

    // plugins needed at runtime but not for compilation
    runtime ":hibernate4:4.3.5.5" // or ":hibernate:3.6.10.17"
    runtime ":database-migration:1.4.0"
    runtime ":jquery:1.11.1"
    compile ":console:1.3"
    // Uncomment these to enable additional asset-pipeline capabilities
    //compile ":sass-asset-pipeline:1.9.0"
    //compile ":less-asset-pipeline:1.10.0"
    //compile ":coffee-asset-pipeline:1.8.0"
    //compile ":handlebars-asset-pipeline:1.3.0.3"
    compile ':spring-security-core:2.0-RC4'
    compile 'bgl.releases:spring-security-oauth2-provider:1.0.5-G'
    test ":geb:0.9.3"
    compile(":build-test-data:2.1.2") {
      export = false
      if (Environment.getCurrent() in [
        Environment.TEST,
        Environment.DEVELOPMENT]
      ) {
        export = true
      }
    }
    // Testing
    test ':code-coverage:2.0.3-2', { export = false }
    test ':codenarc:0.21', { export = false }

    //compile ':webxml:1.4.1'
  }
}
