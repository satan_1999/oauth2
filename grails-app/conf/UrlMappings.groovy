class UrlMappings {

  static mappings = {
    "/$controller/$action?/$id?"{ constraints {
        // apply constraints here
      } }

    "/login/login?"(controller: "login",action:"index")
    "/login/$action?"(controller: "login")
    "/logout/$action?"(controller: "logout")
    "/logout/logout?"(controller: "logout",action:"index")
    "/oauth/authorize"(uri:"/oauth/authorize.dispatch")
    "/oauth/token"(uri:"/oauth/token.dispatch")

    "/"(view:"/index")
    "500"(view:'/error')
  }
}
