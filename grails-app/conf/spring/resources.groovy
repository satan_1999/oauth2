

import grails.plugin.springsecurity.oauthprovider.OAuth2AuthenticationSerializer
import au.com.bgl.api.oauthprovider.ApiAuthorizationCodeService
import au.com.bgl.api.oauthprovider.ApiClientDetailsService
import au.com.bgl.api.oauthprovider.ApiTokenStoreService

// Place your Spring DSL code here
//import springsecurity.oauthprovider.GormClientDetailsService
//import springsecurity.oauthprovider.GormTokenStoreService
  
beans = {
  oauth2AuthenticationSerializer(OAuth2AuthenticationSerializer)
  gormClientDetailsService(ApiClientDetailsService)
  gormTokenStoreService(ApiTokenStoreService){
    oauth2AuthenticationSerializer = ref('oauth2AuthenticationSerializer')
  }
  gormAuthorizationCodeService(ApiAuthorizationCodeService){
    oauth2AuthenticationSerializer = ref('oauth2AuthenticationSerializer')
  }
//  springConfig.addAlias 'clientDetailsService', 'clientDetailsService'
//  springConfig.addAlias 'tokenStore', 'tokenStore'
//  springConfig.addAlias 'authorizationCodeServices', 'authorizationCodeServices'


}
