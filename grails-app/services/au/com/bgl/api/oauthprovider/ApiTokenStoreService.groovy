package au.com.bgl.api.oauthprovider

import java.util.Collection;

import grails.plugin.springsecurity.SpringSecurityUtils

import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken
import org.springframework.security.oauth2.common.OAuth2AccessToken
import org.springframework.security.oauth2.common.OAuth2RefreshToken
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.token.TokenStore
import org.springframework.security.oauth2.common.util.SerializationUtils;
import org.springframework.security.oauth2.common.ExpiringOAuth2RefreshToken

import au.com.bgl.api.auth.ApiAccessToken
import au.com.bgl.api.auth.ApiRefreshToken

class ApiTokenStoreService implements TokenStore {

  def oauth2AuthenticationSerializer 

  @Override
  OAuth2Authentication readAuthentication(OAuth2AccessToken token) {
    log.debug "calling readAuthentication"
    return readAuthentication(token.value)
//    ApiAccessToken accessToken = ApiAccessToken.findByValue(token.getValue())
//    if (accessToken != null) {
//      return SerializationUtils.deserialize(accessToken.authentication)
//    } else {
//      return null
//    }
  }

  @Override
  void storeAccessToken(OAuth2AccessToken token, OAuth2Authentication authentication) {
    log.debug "calling storeAccessToken"
    def ctorArgs = [
                authentication: oauth2AuthenticationSerializer.serialize(authentication),
                username: authentication.isClientOnly() ? null : authentication.name,
                clientId: authentication.getOAuth2Request().clientId,
                value: token.value,
                tokenType: token.tokenType,
                expiration: token.expiration,
                refreshToken: token.refreshToken?.value,
                scope: token.scope
        ]
        ApiAccessToken.newInstance(ctorArgs).save(failOnError: true)
  }

  @Override
  OAuth2AccessToken readAccessToken(String tokenValue) {
    log.debug "calling readAccessToken"
    ApiAccessToken token = ApiAccessToken.findByValue(tokenValue)
    if (token != null) {
       createOAuth2AccessToken(token)
    } else {
      return null
    }
    
//    @Override
//    OAuth2AccessToken readAccessToken(String tokenValue) {
//        def (accessTokenLookup, GormAccessToken) = getAccessTokenLookupAndClass()
//
//        def valuePropertyName = accessTokenLookup.valuePropertyName
//        def gormAccessToken = GormAccessToken.findWhere((valuePropertyName): tokenValue)
//
//        if (!gormAccessToken) {
//            log.debug("Failed to find access token with value [$tokenValue]")
//            return null
//        }
//        createOAuth2AccessToken(gormAccessToken)
//    }
  }

  void removeAccessToken(String tokenValue) {
    log.debug "calling removeAccessToken"
    ApiAccessToken.findByValue(tokenValue)?.delete()
  }

  OAuth2Authentication readAuthentication(ExpiringOAuth2RefreshToken token) {
    ApiRefreshToken refreshToken = ApiRefreshToken.findByValue(token.getValue())
    if (refreshToken != null) {
      return SerializationUtils.deserialize(refreshToken.authentication)
    } else {
      return null
    }
  }

  void storeRefreshToken(ExpiringOAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
    new ApiRefreshToken (
        tokenId: refreshToken.getValue(),
        token: SerializationUtils.serialize(refreshToken),
        authentication: SerializationUtils.serialize(authentication)
        ).save(failOnError: true)
  }

  @Override
  ExpiringOAuth2RefreshToken readRefreshToken(String tokenValue) {
    ApiRefreshToken token = ApiRefreshToken.findByValue(tokenValue)
    if (token != null) {
      return SerializationUtils.deserialize(token.value)
    } else {
      return null
    }
  }

  void removeRefreshToken(String tokenValue) {
    log.debug "calling removeRefreshToken"
    ApiRefreshToken token = ApiRefreshToken.findByValue(tokenValue)
    if (token != null) {
      token.delete()
    }
  }

  void removeAccessTokenUsingRefreshToken(String refreshToken) {
    log.debug "calling removeAccessTokenUsingRefreshToken"
    ApiAccessToken token = ApiAccessToken.findByRefreshToken(refreshToken)
    if (token != null) {
      token.delete()
    }
  }

  @Override
  public Collection<OAuth2AccessToken> findTokensByClientId(String clientId) {
    return ApiAccessToken.findAllByClientId(clientId)
  }



  @Override
  public OAuth2AccessToken getAccessToken(OAuth2Authentication authentication) {
    log.debug "calling getAccessToken"
    def serializedAuthentication = oauth2AuthenticationSerializer.serialize(authentication)
    def apiAccessToken = ApiAccessToken.findByAuthentication( serializedAuthentication)

    if (!apiAccessToken) {
      log.debug("Failed to find access token for authentication [$authentication]")
      return null
    }
    return createOAuth2AccessToken(apiAccessToken)
  }

  @Override
  public OAuth2Authentication readAuthentication(String token) {
    log.debug "calling readAuthentication"
    OAuth2Authentication authentication = null
    try {
      def apiAccessToken = ApiAccessToken.findByValue( token)
      authentication = oauth2AuthenticationSerializer.deserialize(apiAccessToken.authentication)
    }
    catch (IllegalArgumentException e) {
      log.warn("Failed to deserialize authentication for access token [$token]",e)
      removeAccessToken(token)
    }
    return authentication
  }

  @Override
  public OAuth2Authentication readAuthenticationForRefreshToken(OAuth2RefreshToken token) {
    log.debug "calling readAuthenticationForRefreshToken"
    OAuth2Authentication authentication = null
    String tokenValue = token.value

    try {
      def refreshToken = ApiRefreshToken.findByValue(tokenValue)
      authentication = oauth2AuthenticationSerializer.deserialize(refreshToken.authentication)
    }
    catch (IllegalArgumentException e) {
      log.warn("Failed to deserialize authentication for refresh token [${tokenValue}]", e)
      removeRefreshToken(tokenValue)
    }
    return authentication
  }

  @Override
  public void removeAccessToken(OAuth2AccessToken token) {
    log.debug "calling removeAccessToken"
    removeAccessToken(token.value)
  }

  @Override
  public void removeAccessTokenUsingRefreshToken(OAuth2RefreshToken refreshToken) {
    log.debug "calling removeAccessTokenUsingRefreshToken"
    ApiAccessToken.findByRefreshToken(refreshToken.value)?.delete()
  }

  @Override
  void removeRefreshToken(OAuth2RefreshToken token) {
    removeRefreshToken(token.value)
  }

  @Override
  public void storeRefreshToken(OAuth2RefreshToken refreshToken, OAuth2Authentication authentication) {
    def ctorArgs = [
      authentication: oauth2AuthenticationSerializer.serialize(authentication),
      value: refreshToken.value,
    ]
    new ApiRefreshToken(ctorArgs).save()
  }



  private OAuth2AccessToken createOAuth2AccessToken(ApiAccessToken  apiAccessToken) {
    def token = new DefaultOAuth2AccessToken(apiAccessToken.value)
    token.refreshToken = createRefreshTokenForAccessToken(apiAccessToken)
    token.tokenType = apiAccessToken.tokenType
    token.expiration = apiAccessToken.expiration
    token.scope = apiAccessToken.scope
    return token
  }

  private OAuth2RefreshToken createRefreshTokenForAccessToken(ApiAccessToken  apiAccessToken) {
    def refershToken = ApiRefreshToken.findOrCreateByValue(apiAccessToken.refreshToken)
    return refershToken ? new DefaultOAuth2RefreshToken(apiAccessToken.refreshToken) : null
    
  }

  @Override
  public Collection<OAuth2AccessToken> findTokensByClientIdAndUserName(String clientId, String userName) {
    return ApiAccessToken.findAllByClientIdAndUsername(clientId, userName)
  }
  
}
