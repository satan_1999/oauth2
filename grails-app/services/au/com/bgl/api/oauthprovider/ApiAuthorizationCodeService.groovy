package au.com.bgl.api.oauthprovider

import au.com.bgl.api.auth.ApiAuthorizationCode
import grails.transaction.Transactional

import org.codehaus.groovy.grails.commons.GrailsApplication
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.code.RandomValueAuthorizationCodeServices
import org.springframework.util.Assert
@Transactional
class ApiAuthorizationCodeService extends RandomValueAuthorizationCodeServices {

  def oauth2AuthenticationSerializer
  def grailsApplication

  @Override
  protected void store(String code, OAuth2Authentication authentication) {
    log.debug "store $code"
    Assert.notNull authentication
    Assert.notNull code
    def ctorArgs = [
      code: code,
      authentication: oauth2AuthenticationSerializer.serialize(authentication)
    ]

    new ApiAuthorizationCode(ctorArgs).save(flush:true)
  }
  
  @Override
  protected OAuth2Authentication remove(String code) {
    log.debug "remove $code"
    def apiAuthorizationCode = ApiAuthorizationCode.findByCode(code)

    if(!apiAuthorizationCode){
      log.warn("Not find apiAuthorizationCode by code $code")
      return null
    }
    OAuth2Authentication authentication = null

    try {
      authentication = oauth2AuthenticationSerializer.deserialize(apiAuthorizationCode?.authentication)
    }
    catch(IllegalArgumentException e) {
      log.warn("Failed to deserialize authentication for code [$code]",e)
      authentication = null
    }
    finally {
      apiAuthorizationCode?.delete()
    }

    return authentication
  }



}