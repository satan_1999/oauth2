package au.com.bgl.api.oauthprovider

import grails.plugin.springsecurity.SpringSecurityUtils

import org.springframework.security.oauth2.provider.ClientDetails
import org.springframework.security.oauth2.provider.ClientDetailsService
import org.springframework.security.oauth2.provider.ClientRegistrationException
import org.springframework.security.oauth2.provider.NoSuchClientException
import org.springframework.security.oauth2.provider.client.BaseClientDetails

import au.com.bgl.api.auth.ApiClient

class ApiClientDetailsService implements ClientDetailsService {


    @Override
    ClientDetails loadClientByClientId(String clientId) throws ClientRegistrationException {
      log.debug "loadClientByClientId"

        def client = ApiClient.findByClientId(clientId)
        if(client == null) {
            throw new NoSuchClientException("No client with requested id: $clientId")
        }
        return createClientDetails(client)
    }



    private ClientDetails createClientDetails(ApiClient c) {

        ApiClient client = c.refresh()
        // Load client properties or defaults
        def resourceIds = client.resourceIds
        def scopes = client.scopes
        def authorizedGrantTypes = client.authorizedGrantTypes
        def redirectUris = client.redirectUris

        def clientId = client.clientId
        def authorities = client.authorities

        def details = new BaseClientDetails(clientId, csv(resourceIds), csv(scopes), csv(authorizedGrantTypes), csv(authorities), csv(redirectUris))
        details.clientSecret = client.clientSecret
        details.accessTokenValiditySeconds  = client.accessTokenValiditySeconds
        details.refreshTokenValiditySeconds = client.refreshTokenValiditySeconds
        details.additionalInformation = client.additionalInformation
        correctAuthorizedGrantTypes(details, authorizedGrantTypes)
        return details
    }

    /*
        The constructor for BaseClientDetails defaults the authorized grant types to authorization_code
        and refresh_token. We want the developer to have final say on what grant types a client should have.
        Thus we need to ensure that the ClientDetails we return only contain the authorized grant types that
        either the client or the default config allows.
      */
    private void correctAuthorizedGrantTypes(BaseClientDetails clientDetails, Collection<String> authorizedGrantTypes) {
        clientDetails.authorizedGrantTypes = authorizedGrantTypes
    }

    private static String csv(Collection collection) {
        collection?.join(',')
    }
}
